$(function() { // document.ready
  $("#deleteEntryButton").click(function() {
    return confirm("Wirklich löschen?");
  });

  $("#editFormSubmit").click(function() {
    $("#editForm").submit();
  });
});
