<?php
session_start();

require_once '../lib/includes/user.inc.php';
require_once __DIR__.'/../lib/includes/helper.inc.php';
require_once __DIR__.'/../lib/includes/header.inc.php';
require_once __DIR__.'/../lib/ConnectionHandler.php';
require_once __DIR__.'/../lib/database/BlogModel.php';
require_once __DIR__.'/../lib/database/CommentModel.php';
require_once __DIR__.'/../lib/database/UserModel.php';
require_once __DIR__.'/../lib/database/CategoryModel.php';
require_once __DIR__.'/../lib/database/EntryModel.php';
require_once __DIR__.'/../lib/database/Join.php';
$config = include __DIR__.'/../lib/includes/config.inc.php';

if (!validateLoggedIn()) {
    redirectRegister('../');
}

$BlogModel = new BlogModel();
$UserModel = new UserModel();
$CategoryModel = new CategoryModel();
$CommentModel = new CommentModel();
$EntryModel = new EntryModel();

$categories = $CategoryModel->readAll();

$user = $UserModel->currentUser();
$id = $_GET['id'];

$entryJoins = array(
  new Join('category', 'entry', 'categoryId', 'categoryId'),
  new Join('blog', 'entry', 'blogId', 'blogId'),
  new Join('user', 'blog', 'userId', 'userId'),
);
$entry = $EntryModel->joinAndReadAll($entryJoins, 'entryId = '.$id)[0];

$commentJoins = array(
  new Join('user', 'comment', 'userId', 'userId'),
);

$comments = $CommentModel->joinAndReadAll($commentJoins, 'entryId = '.$id, 100, '*', false, 'commentId desc');
?>
<!DOCTYPE html>
<html lang="de">
<head>
  <?php printHeader("{$entry['name']} - ".strTruncate($entry['title'], 20), '../') ?>

  <link rel="stylesheet" href="../css/entry-view-app.css">
  <script src="../js/entry-view_app.js"></script>
</head>
<body>
<div class="container">
    <h1 class="page-header"><?= $entry['name']?> <small><?= strTruncate($entry['title'], 50)?></small></h1>

    <ul class="breadcrumb">
      <li><a href="../index.php">Blogübersicht</a></li>
      <li><a href="../blog/view.php?id=<?= $entry['blogId']?>"><?= $entry['name'] ?></a></li>
      <li class="active"><?= strTruncate($entry['title'], 30) ?></li>
    </ul>

    <?php printUser('../');
    if (userIsAuthorized($entry['blogId'])) {
        ?>
    <a class="btn btn-sm btn-primary" data-toggle="modal" data-target="#editEntryModal">Eintrag bearbeiten</a>

<form action="../blog/performBlogAction.php" method="post" class="inline-block">
  <input type="hidden" name="type" value="deleteEntry">
  <input type="hidden" name="entryId" value="<?= $entry['entryId'] ?>">
  <button type="submit" class="btn btn-sm btn-danger" id="deleteEntryButton">Eintrag löschen</button>
</form>

    <?php
    } require '../lib/includes/flash.inc.php'; ?>

    <h3><?= $entry['title']?></h3>

    <div class="container-fluid blog-attributes">
      <ul class="fa-ul">
        <li><i class="fa fa-li fa-user"></i><?= $entry['nickname'] ?></li>
        <li><i class="fa fa-li fa-hashtag"></i><?= $entry[7] ?></li>
        <li><i class="fa fa-li fa-clock-o"></i><?= $entry['date'] ?></li>
      </ul>
    </div>

    <p>
      <?= nl2br($entry['content']) ?>
    </p>

    <hr>
    <h4><?= count($comments)?> Kommentar<?php if (count($comments) !== 1) {
    echo 'e';
}?></h4>

    <form class="margin-btm-10" action="performEntryAction.php" method="post" id="addCommentForm">
      <input type="hidden" name="type" value="addComment">
      <input type="hidden" name="entryId" value="<?= $entry['entryId'] ?>">
      <div class="row">
        <div class="form-group col-lg-6 col-md-6">
          <label class="control-label" for="addCommentComment">Kommentar</label>
          <textarea class="form-control" id="addCommentComment" type="text" name="comment" placeholder="Neuen Kommentar schreiben"></textarea>
        </div>
      </div>
      <input class="btn btn-md btn-primary" type="submit" value="Kommentar absenden">
    </form>

    <?php foreach ($comments as $comment) {
    ?>
      <b><?= $comment['nickname'] ?> (<?= $comment['date'] ?>): <?php if (userIsAuthorizedForComment($comment['commentId'])) {
    echo '<a href="performEntryAction.php?type=deleteComment&commentId='.$comment['commentId'].'"><i class="fa fa-times"></i></a>';
}
    ?></b>
      <p>
        <?= $comment['comment'] ?>
      </p>
    <?php
} ?>

</div>


<!-- EDIT ENTRY MODAL -->
<div class="modal fade" id="editEntryModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
                <h4 class="modal-title">Eintrag bearbeiten</h4>
            </div>
            <div class="modal-body">
                <form action="../blog/performBlogAction.php" method="POST" id="editForm">
                    <input type="hidden" name="type" value="edit"/>
                    <input type="hidden" name="blogId" value="<?= $entry['blogId'] ?>">
                    <input type="hidden" name="entryId" value="<?= $entry['entryId'] ?>">

                    <div class="form-group">
                      <label for="createFormSelectCategory">Kategorie</label>
                      <select class ="form-control" name="categoryId" id="createFormSelectCategory">
                        <?php foreach ($categories as $category):?>
                          <option value="<?= $category['categoryId'] ?>" <?php if ($category['categoryId'] == $entry['categoryId']) {
    echo 'selected';
} ?>><?= $category['name'] ?></option>
                        <?php endforeach; ?>
                      </select>
                    </div>


                    <div class="form-group">
                        <label for="createFormTitle" class="control-label">Titel</label>
                        <input type="text" name="title" id="createFormTitle" class="form-control" placeholder="Titel" value="<?= htmlspecialchars_decode($entry['title']) ?>" required>
                    </div>
                    <div class="form-group">
                        <label for="createFormContent" class="control-label">Inhalt</label>
                        <textarea name="content" id="createFormContent" class="form-control" placeholder="Inhalt" required><?= htmlspecialchars_decode($entry['content']) ?></textarea>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
                <button type="button" class="btn btn-primary" id="editFormSubmit">Eintrag bearbeiten</button>
            </div>
        </div>
    </div>
</div>

</body>
</html>
