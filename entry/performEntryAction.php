<?php

require_once __DIR__.'/../lib/includes/helper.inc.php';
require_once __DIR__.'/../lib/ConnectionHandler.php';
require_once __DIR__.'/../lib/database/EntryModel.php';
require_once __DIR__.'/../lib/database/CommentModel.php';

session_start();

if (!validateLoggedIn()) {
    redirectRegister('../');
}

$EntryModel = new EntryModel();
$CommentModel = new CommentModel();

$type = '';

if (!isset($_POST['type'])) {
    if (!isset($_GET['type'])) {
        echo 'custom request';
        die();
    } else {
        $type = $_GET['type'];
    }
} else {
    $type = $_POST['type'];
}

switch ($type) {
  case 'addComment':
  if (!assureNonEmpty('POST', 'entryId')) {
      header('Location: ../index.php'.generateFlash('Interner Fehler: err_entryid_not_set', 'danger'));
      die();
  }

  $entryId = $_POST['entryId'];

  if (!assureNonEmpty('POST', 'comment')) {
      header('Location: view.php'.generateFlash('Kein Kommentar gesendet.', 'warning')."&id=$entryId");
      die();
  }

  $query = 'INSERT INTO comment (userId, entryId, comment, date) values (:userId, :entryId, :comment, :date)';
  $stmt = ConnectionHandler::getConnection()->prepare($query);
  $stmt->bindParam(':userId', $_SESSION['userId'], SQLITE3_INTEGER);
  $stmt->bindParam(':entryId', $entryId, SQLITE3_INTEGER);
  $stmt->bindParam(':comment', htmlspecialchars($_POST['comment']), SQLITE3_TEXT);
  $stmt->bindParam(':date', date(DATE_ATOM), SQLITE3_TEXT);

  $result = $stmt->execute();

  if ($result) {
      header('Location: view.php'.generateFlash('Kommentar hinzugefügt.', 'success')."&id=$entryId");
  } else {
      header('Location: ../index.php'.generateFlash('Interner Fehler: err_cant_insert_database', 'danger')."&id={$entryId}");
  }

  break;
  case 'deleteComment':
    if (!assureNonEmpty('GET', 'commentId')) {
        header('Location: ../index.php'.generateFlash('Interner Fehler: err_commentid_not_set', 'danger'));
        die();
    }

    $commentId = $_GET['commentId'];

    $query = 'SELECT entryId from comment where commentId = :commentId';
    $stmt = ConnectionHandler::getConnection()->prepare($query);
    $stmt->bindParam(':commentId', $commentId, SQLITE3_INTEGER);
    $result = $stmt->execute();
    $resultArray = $result->fetchArray();

    $entryId = $resultArray[0];

    if (!userIsAuthorizedForComment($commentId)) {
        header('Location: view.php'.generateFlash('Du bist nicht berechtigt diesen Kommentar zu löschen', 'info')."&id=$entryId");
        die();
    }

    $CommentModel->deleteById($commentId);

    header('Location: view.php'.generateFlash('Kommentar gelöscht', 'success')."&id=$entryId");
  break;
}
