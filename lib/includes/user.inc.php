<?php
require_once __DIR__."/../database/UserModel.php";
require_once __DIR__."/helper.inc.php";

$UserModel = new UserModel();

function printUser($path = "./") {
  global $UserModel;
 ?>
<p>Hallo <?php if (userIsAdmin()): echo '<i class="fa fa-wrench"></i> '; endif; ?><?= $UserModel->currentUser()['nickname'] ?>! <a href="<?= $path ?>user/performUserAction.php?type=logout"
                                                          class="btn btn-sm btn-link">Abmelden</a></p>
<?php
}
?>
