<?php
require_once __DIR__.'/helper.inc.php';

$validation = validateFlash();

if ($validation) {
    ?>
    <div class="row">
        <div class="alert alert-<?= $_GET['flashType'] ?>">
            <h4>Information</h4>

            <p><?= $_GET['flash'] ?></p>
        </div>
    </div>
<?php
} ?>
