<?php
require_once __DIR__.'/../ConnectionHandler.php';
require_once __DIR__.'/../database/UserModel.php';
require_once __DIR__.'/../database/BlogModel.php';
require_once __DIR__.'/../database/CommentModel.php';
require_once __DIR__.'/../database/Join.php';

$config = include __DIR__.'/config.inc.php';

$UserModel = new UserModel();
$BlogModel = new BlogModel();
$CommentModel = new CommentModel();

function generateFlash($message, $type = 'warning')
{
    global $config;
    $time = time();

    return '?flash='.urlencode($message).'&flashHash='.hash('sha256', $message.$config['secret'].$time.$type)."&flashTime=$time&flashType=$type";
}

/**
 * @return bool
 */
function validateFlash()
{
    global $config;

    if (!isset($_GET['flash']) || !isset($_GET['flashHash']) || !isset($_GET['flashTime']) || !isset($_GET['flashType'])) {
        return false;
    }

    $message = $_GET['flash'];
    $hash = $_GET['flashHash'];
    $time = $_GET['flashTime'];
    $type = $_GET['flashType'];

    $currentTime = time();
    if (!($currentTime - 1 <= $time && $time <= $currentTime + 1)) {
        return false;
    }

    return hash('sha256', $message.$config['secret'].$time.$type) === $hash;
}

/**
 * Checks if a session had a valid logged in user assigned to it.
 *
 * @param bool $dbValidate true if the application should check the database for the existence of the user
 *
 * @return bool True if a valid user is logged in
 */
function validateLoggedIn($dbValidate = true)
{
    $userId = isset($_SESSION['userId']) ? $_SESSION['userId'] : null;

    if ($userId && $dbValidate) {
        $query = 'SELECT COUNT(*) FROM user WHERE userId = :id';
        $stmt = ConnectionHandler::getConnection()->prepare($query);
        $stmt->bindValue(':id', $userId, SQLITE3_INTEGER);
        $result = $stmt->execute();

        return $result ? $result->fetchArray(SQLITE3_NUM)[0] == 1 : false;
    } else {
        return boolval($userId);
    }
}

function redirectHome($relation)
{
    header("Location: ${relation}index.php");
}

function redirectRegister($relation)
{
    header("Location: ${relation}user/loginRegister.php".generateFlash('Sie müssen eingeloggt sein um diese Seite aufzurufen', 'info'));
}
/**
 *  @param str $string
 *  @param int $maxLength
 */
function strTruncate($string, $maxLength, $end = '...')
{
    return (strlen($string) > $maxLength ? substr($string, 0, ($maxLength - strlen($end))).$end : $string);
}

/**
 * Checks if the session user is authorized to perform administrative tasks on the given blogId.
 *
 * @param int $blogId The blog id to check permissions for
 *
 * @return bool
 */
function userIsAuthorized($blogId)
{
    global $UserModel, $BlogModel;

    $userId = $_SESSION['userId'];
    $user = $UserModel->readById($userId);
    $blog = $BlogModel->readById($blogId);

    return $blog['userId'] == $user['userId'] || $user['roleId'] == 2; // 1 = User, 2 = Admin
}

/**
 * Checks if the session user is an admin.
 *
 * @return bool
 */
function userIsAdmin()
{
    global $UserModel;

    $userId = $_SESSION['userId'];
    $user = $UserModel->readById($userId);

    return $user['roleId'] == 2; // 1 = User, 2 = Admin
}

function userIsAuthorizedForComment($commentId)
{
    global $CommentModel;

    $joins = array(
      new Join('entry', 'comment', 'entryId', 'entryId'),
      new Join('blog', 'entry', 'blogId', 'blogId'),
      new Join('user', 'comment', 'userId', 'userId'),
    );

    $commentId = SQLite3::escapeString($commentId);
    $comment = $CommentModel->joinAndReadAll($joins, "commentId = $commentId", 1, 'comment.userId as userId, blog.blogId as blogId')[0];
    $userId = $_SESSION['userId'];

    return userIsAdmin() || userIsAuthorized($comment['blogId']) || $userId === $comment['userId'];
}

function assureNonEmpty($method, ...$variables)
{
    $var = "_$method";
    global ${$var};
    $methodArr = ${$var};

    foreach ($variables as $variable) {
        if (!isset($methodArr[$variable]) || empty($methodArr[$variable]) || !trim($methodArr[$variable])) {
            return false;
        }
    }

    return true;
}
