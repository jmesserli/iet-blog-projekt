<?php

$config = require __DIR__.'/config.inc.php';

function printHeader($title, $path = './')
{
    global $config;

    echo <<<EOF
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>{$config['brand']} - {$title}</title>

  <link rel="stylesheet" href="{$path}ext/bootstrap/bootstrap.min.css">
  <link rel="stylesheet" href="{$path}ext/font-awesome/css/font-awesome.min.css">
  <link rel="shortcut icon" href="{$path}img/pen-favicon.png">

  <script src="{$path}ext/jquery/jquery.min.js"></script>
  <script src="{$path}ext/bootstrap/bootstrap.min.js"></script>

  <!--[if lt IE 9]>
  <script src="{$path}ext/html5shiv/html5shiv.min.js"></script>
  <script src="{$path}ext/respond/respond.min.js"></script>
  <![endif]-->

EOF;
}
