<?php

require_once "database/DatabaseHelper.php";

class ConnectionHandler
{
    private static $connection = null;


    /**
     * @return SQLite3
     */
    public static function getConnection()
    {
        if (ConnectionHandler::$connection === null)
            ConnectionHandler::$connection = new DatabaseHelper();

        return ConnectionHandler::$connection;
    }

}