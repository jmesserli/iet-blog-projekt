<?php


class DatabaseHelper extends SQLite3
{
    private $config;

    /**
     * DatabaseHelper constructor.
     */
    public function __construct()
    {
        $this->config = include __DIR__.'/../includes/config.inc.php';
        $this->open(__DIR__.'/../../'.$this->config['dbFile']);
    }
}
