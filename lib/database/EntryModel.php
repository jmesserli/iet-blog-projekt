<?php

require_once __DIR__.'/BaseModel.php';

class EntryModel extends BaseModel
{
    public function __construct()
    {
        parent::__construct('entry');
    }
}
