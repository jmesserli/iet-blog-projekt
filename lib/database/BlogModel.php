<?php

require_once __DIR__ . "/BaseModel.php";

class BlogModel extends BaseModel
{
    public function __construct()
    {
        parent::__construct("blog");
    }

    public function create($userId, $name)
    {
        $query = "INSERT INTO blog (userId, name) VALUES (:userid, :name)";
        $stmt = ConnectionHandler::getConnection()->prepare($query);
        $stmt->bindParam(":userid", $userId, SQLITE3_INTEGER);
        $stmt->bindParam(":name", $name, SQLITE3_TEXT);
        return $stmt->execute();
    }
}