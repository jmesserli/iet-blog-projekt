<?php

require_once __DIR__.'/BaseModel.php';

class CommentModel extends BaseModel
{
  public function __construct() {
    parent::__construct("comment");
  }

}
