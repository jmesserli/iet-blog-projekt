<?php

require_once __DIR__.'/../ConnectionHandler.php';

class BaseModel
{
    protected $tableName = null;

    public function __construct($tableName)
    {
        $this->tableName = $tableName;
    }

    public function readById($id)
    {
        $query = "SELECT * FROM $this->tableName WHERE {$this->tableName}Id = :id";

        $statement = ConnectionHandler::getConnection()->prepare($query);
        $statement->bindValue(':id', $id, SQLITE3_INTEGER);

        $result = $statement->execute();
        $row = $result->fetchArray(SQLITE3_BOTH);
        $statement->close();

        return $row;
    }

    public function readAll($max = 100, $orderBy = 'default order')
    {
        if ($orderBy == 'default order') {
            $orderBy = "{$this->tableName}Id asc";
        } // "order by {tableName}Id asc" by default

        $query = "SELECT * FROM $this->tableName ORDER BY $orderBy LIMIT 0, $max";

        $statement = ConnectionHandler::getConnection()->prepare($query);
        $result = $statement->execute();

        $rows = [];
        while ($row = $result->fetchArray()) {
            $rows[] = $row;
        }

        $statement->close();

        return $rows;
    }

    public function deleteById($id)
    {
        $query = "DELETE FROM $this->tableName WHERE {$this->tableName}Id = :id";

        $statement = ConnectionHandler::getConnection()->prepare($query);
        $statement->bindParam(':id', $id, SQLITE3_INTEGER);
        $statement->execute();
    }

    public function updateFieldById($id, $field, $new_value)
    {
        $query = "UPDATE {$this->tableName} SET $field = :value WHERE {$this->tableName}Id = :id";

        $stmt = ConnectionHandler::getConnection()->prepare($query);
        $stmt->bindParam(':value', $new_value);
        $stmt->bindParam(':id', $id, SQLITE3_INTEGER);

        return $stmt->execute();
    }

    public function getLastEntry()
    {
        return $this->readAll(1, "{$this->tableName}Id desc")[0];
    }

    public function joinAndReadAll($joinsArray, $where = "'1'='1'", $limit = 100, $select = '*', $debug = false, $orderby = '')
    {
        $query = "SELECT $select FROM {$this->tableName} ";

        foreach ($joinsArray as $join) {
            $query .= $join;
        }
        $query .= "WHERE $where ";

        if ($orderby != '') {
            $query .= "ORDER BY $orderby ";
        }

        $query .= " LIMIT 0, $limit";

        if ($debug) {
            echo $query;
        }

        $stmt = ConnectionHandler::getConnection()->prepare($query);
        if (!$stmt) {
          return null;
        }
        $res = $stmt->execute();

        $rows = [];
        while ($row = $res->fetchArray()) {
            $rows[] = $row;
        }

        $stmt->close();

        return $rows;
    }

    public function readByFilter($filter, $select = '*')
    {
        $query = "SELECT $select FROM {$this->tableName} WHERE ".$filter;
        $stmt = ConnectionHandler::getConnection()->prepare($query);
        $result = $stmt->execute();

        $rows = [];
        while ($row = $result->fetchArray()) {
            $rows[] = $row;
        }

        $stmt->close();

        return $rows;
    }
}
