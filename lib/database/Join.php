<?php

class Join
{
    public $pkTable, $fkTable, $pkName, $fkName;

    /**
     * Constructor for Join. Generated SQL: JOIN $pkTable ON $fkTable.$fkName = $pkTable.$pkName
     * @param $pkTable  string The name of the table which contains the original primary key
     * @param $fkTable  string The name of the table which contains the foreign key
     * @param $pkName   string The name of the attribute to use to join inside the $targetTable
     * @param $fkName   string The name of the attribute to use to join inside the $fkTable
     */
    function __construct($pkTable, $fkTable, $pkName, $fkName)
    {
        $this->pkTable = $pkTable;
        $this->fkTable = $fkTable;
        $this->pkName = $pkName;
        $this->fkName = $fkName;
    }

    function __toString()
    {
        return " JOIN $this->pkTable ON $this->fkTable.$this->fkName = $this->pkTable.$this->pkName ";
    }
}