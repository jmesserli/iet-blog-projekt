<?php
require_once __DIR__."/BaseModel.php";

class UserModel extends BaseModel
{
    public function __construct()
    {
        parent::__construct("user");
    }

    public function currentUser()
    {
        $id = $_SESSION["userId"];

        $query = "SELECT * FROM user WHERE userId = :id";
        $stmt = ConnectionHandler::getConnection()->prepare($query);
        $stmt->bindValue(":id", $id, SQLITE3_INTEGER);
        $result = $stmt->execute();

        return $result->fetchArray();
    }
}
