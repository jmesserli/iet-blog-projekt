<?php

require_once __DIR__.'/BaseModel.php';

class CategoryModel extends BaseModel
{
    public function __construct()
    {
        parent::__construct('category');
    }
}
