<?php
require_once 'lib/includes/helper.inc.php';
require_once "lib/includes/header.inc.php";
require_once "lib/includes/user.inc.php";
$config = include 'lib/includes/config.inc.php';
session_start();

if (!validateLoggedIn()) {
    // user needs to log in / register
    header('Location: user/loginRegister.php');
    die();
}

require_once 'lib/database/Join.php';
require_once 'lib/database/BlogModel.php';
require_once 'lib/database/UserModel.php';

$UserModel = new UserModel();
$BlogModel = new BlogModel();

$user = $UserModel->currentUser();
?>

<!DOCTYPE html>
<html lang="de">
<head>
    <?php printHeader("Blogübersicht") ?>

    <script src="js/index_app.js"></script>
</head>
<body>
<div class="container">
    <h1 class="page-header"><?= $config['brand'] ?>
        <small>Blogübersicht</small>
    </h1>
    <ul class="breadcrumb">
      <li class="active">Blogübersicht</li>
    </ul>

    <?php
    printUser();
    // Display flash
    include __DIR__.'/lib/includes/flash.inc.php';

    $joins = array(new Join('user', 'blog', 'userId', 'userId'));
    $blogs = $BlogModel->joinAndReadAll($joins, "'1'='1'", 1000, '*', false, 'nickname asc, name asc');

    if (!$blogs) {
        ?>
        <div class="jumbotron">
            <h1>Noch keine Blogs.</h1>

            <p>Erstelle den ersten Blog und erzähle der Welt was bei dir läuft!</p>

            <p><a class="btn btn-primary btn-lg" data-toggle="modal" data-target="#createBlogModal">Blog erstellen</a>
            </p>
        </div>

    <?php
    } else {
        ?>
        <p><a class="btn btn-primary btn-sm" data-toggle="modal" data-target="#createBlogModal">Blog erstellen</a></p>
        <div class="list-group">
    <?php
    foreach ($blogs as $blog) {
        $blogUser = $UserModel->readById($blog['userId']);
        ?>
    <a href="blog/view.php?id=<?= $blog['blogId'] ?>" class="list-group-item">
      <p class="list-group-item-text">
        <?= $blog['name'] ?> <small>von <span class="<?php if ($user['userId'] == $blogUser['userId']) {
    echo 'label label-success'; } ?>"><?= $blogUser['nickname']?></span></small>
      </p>
    </a>
    <?php } ?>
      </div>
  <?php } ?>
</div>

<!-- CREATE BLOG MODAL -->
<div class="modal fade" id="createBlogModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
                <h4 class="modal-title">Blog erstellen</h4>
            </div>
            <div class="modal-body">
                <form action="blog/performBlogAction.php" method="POST" id="createForm">
                    <input type="hidden" name="type" value="create"/>

                    <div class="form-group">
                        <label for="createFormName" class="control-label">Blog Name</label>
                        <input type="text" name="name" id="createFormName" class="form-control">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
                <button type="button" class="btn btn-primary" id="createFormSubmit">Blog erstellen</button>
            </div>
        </div>
    </div>
</div>

</body>
</html>
