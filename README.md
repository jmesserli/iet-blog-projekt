# IET-Blog-Projekt

Von Joel Messerli

## Installation

Die Installation ist einfach: alle Dateien in diesem Ordner in ein Verzeichnis
auf dem Webserver bewegen.

### Voraussetzungen

Die Voraussetzungen für eine funktionierende Seite sind:

*   PHP 5.6.30 oder neuer: [Xampp 5.6.15 Download](https://www.apachefriends.org/xampp-files/5.6.15/xampp-win32-5.6.15-1-VC11-installer.exe)
*   PHP mit aktivierter SQLite3 Erweiterung *(bei XAMPP standardmässig aktiv)*

## Accounts

**Admin** Account: Benutzername `iet-admin`, Passwort: `admin`

**Test** Account: Benutzername `iet-test`, Passwort: `test`

### Admins

Admins haben die Rechte, alles zu tun. Sie können Einträge und Kommentare auf
allen Blogs löschen. Zudem können sie gesamte Blogs löschen.

---

## Anhang

Mitgeliefert wird die Datei
`Arbeitsjournal_IET-Blog-Projekt_Joel Messerli.pdf`, welche direkt in diesem
Ordner liegt. Diese Enthält das Arbeitsjournal.
