<?php
require_once '../lib/includes/helper.inc.php';
require_once '../lib/includes/header.inc.php';
$config = include '../lib/includes/config.inc.php';
session_start();

if (validateLoggedIn()) {
    // User already logged in
    header('Location: ../index.php');
}
?>

<!doctype html>
<html lang="en">
<head>
  <?php printHeader('Registrieren / Anmelden', '../') ?>
</head>
<body>

<div class="container">
    <h1 class="page-header"><?= $config['brand'] ?>
        <small>Bitte melde dich an :)</small>
    </h1>

    <div class="jumbotron">
        <h1><?= $config['brand'] ?></h1>

        <p>Die Blogging-Plattform der Zukunft</p>

        <p>Erstellen Sie noch heute Ihren Blog oder lesen Sie spannende Blogs anderer Leute!</p>

        <p><a class="btn btn-primary btn-lg" href="#forms" role="button">Anmelden!</a></p>
    </div>

    <?php include '../lib/includes/flash.inc.php'; ?>

    <div class="row" id="forms">
        <div class="col-lg-6 col-mg-6 col-sm-6 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Anmelden</h3>
                </div>
                <div class="panel-body">
                    <form action="performUserAction.php" method="POST" id="loginForm">
                        <input type="hidden" name="type" value="login">

                        <div class="form-group">
                            <label for="loginUsername" class="control-label">Username</label>
                            <input type="text" name="username" id="loginUsername" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="loginPassword" class="control-label">Password</label>
                            <input type="password" name="password" id="loginPassword" class="form-control">
                        </div>
                        <input type="submit" class="btn btn-primary" value="Anmelden">
                    </form>
                </div>
            </div>
        </div>

        <div class="col-lg-6 col-mg-6 col-sm-6 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Registrieren</h3>
                </div>
                <div class="panel-body">

                    <form action="performUserAction.php" method="POST" id="registerForm">
                        <input type="hidden" name="type" value="register">

                        <div class="form-group">
                            <label for="registerUsername" class="control-label">Username</label>
                            <input type="text" name="username" id="registerUsername" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="registerEmail" class="control-label">Email</label>
                            <input type="email" name="email" id="registerEmail" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="registerPassword" class="control-label">Passwort</label>
                            <input type="password" name="password" id="registerPassword" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="registerPasswordRepeat" class="control-label">Passwort wiederholen</label>
                            <input type="password" name="passwordRepeat" id="registerPasswordRepeat"
                                   class="form-control">
                        </div>

                        <input type="submit" class="btn btn-primary" value="Registrieren">
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

</body>
</html>
