<?php

session_start();
include '../lib/ConnectionHandler.php';
include '../lib/includes/helper.inc.php';

$type = isset($_POST['type']) ? $_POST['type'] : $_GET['type'];

switch ($type) {
    case 'login':
        $username = filter_input(INPUT_POST, 'username');
        $password = filter_input(INPUT_POST, 'password');

        $query = 'SELECT * FROM user WHERE nickname = :name';
        $stmt = ConnectionHandler::getConnection()->prepare($query);
        $stmt->bindParam(':name', $username, SQLITE3_TEXT);
        $result = $stmt->execute();
        $user = $result->fetchArray(SQLITE3_ASSOC);

        if ($user != null && password_verify($password, $user['password'])) {
            $_SESSION['userId'] = $user['userId'];
            header('Location: ../');
            exit(1);
        } else {
            header('Location: loginRegister.php'.generateFlash('Die eingegebenen Informationen stimmen nicht. Überprüfe deine Anmeldeinformationen nochmals.'));
            exit(-1);
        }
        break;

    case 'register':
        $errors = [];
        $role = 1;

        // TODO Validation
        if (empty($_POST['username'])) {
            $errors[] = 'Kein Benutzername eingegeben';
        }

        $username = trim($_POST['username']);
        if (strlen($username) == 0) {
            $errors[] = 'Username ist zu kurz';
        }

        if (count($errors) == 0) {
            $query = 'SELECT * FROM user WHERE nickname = :name';
            $stmt = ConnectionHandler::getConnection()->prepare($query);
            $stmt->bindParam(':name', $username, SQLITE3_TEXT);
            $result = $stmt->execute();
            $user = $result->fetchArray();

            if ($user) {
                $errors[] = "Username $username wird bereits verwendet";
            }
        }

        if (empty($_POST['email'])) {
            $errors[] = 'Keine Email eingegeben';
        }

        $email = trim($_POST['email']);
        if (strlen($email) == 0) {
            $errors[] = 'E-Mail ist zu kurz';
        }

        $query = 'SELECT * FROM user WHERE email = :email';
        $stmt = ConnectionHandler::getConnection()->prepare($query);
        $stmt->bindParam(':email', $email, SQLITE3_TEXT);
        $result = $stmt->execute();
        $fetchedEmail = $result->fetchArray();

        if ($fetchedEmail) {
            $errors[] = "Email-Adresse $email wird bereits verwendet";
        }

        if (!empty($_POST['password']) && !empty($_POST['passwordRepeat']) && $_POST['password'] === $_POST['passwordRepeat']) {
            if (count($errors) == 0) {
                $db = ConnectionHandler::getConnection();

                $query = 'INSERT INTO user (email, password, nickname, roleId) VALUES (:email, :password, :nickname, :roleId)';
                $stmt = $db->prepare($query);
                $stmt->bindParam(':email', $email, SQLITE3_TEXT);
                $stmt->bindParam(':password', password_hash($_POST['password'], CRYPT_SHA512), SQLITE3_TEXT);
                $stmt->bindParam(':nickname', $username, SQLITE3_TEXT);
                $stmt->bindParam(':roleId', $role, SQLITE3_INTEGER);
                $result = $stmt->execute();

                if ($result) {
                    $userid = $db->lastInsertRowID();
                    $_SESSION['userId'] = $userid;
                    header('Location: ../'.generateFlash('Erfolgreich registriert.'));
                } else {
                    header('Location: loginRegister.php'.generateFlash('Bei der Registrierung ist ein Fehler aufgetreten.'));
                }
            }
        } else {
            $errors[] = 'Passwörter stimmen nicht überein oder sind leer!';
        }

        $returnString = '';
        if (count($errors) > 0) {
            foreach ($errors as $error) {
                $returnString .= "<p>$error</p>";
            }

            header('Location: loginRegister.php'.generateFlash($returnString, 'danger'));
        }

        break;

    case 'logout':
        unset($_SESSION);
        session_destroy();
        header('Location: loginRegister.php'.generateFlash('Sie wurden erfolgreich abgemeldet.', 'success'));
        break;
}
