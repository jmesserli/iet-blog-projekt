<?php
session_start();

require_once '../lib/includes/user.inc.php';
require_once __DIR__.'/../lib/includes/helper.inc.php';
require_once __DIR__."/../lib/includes/header.inc.php";
require_once __DIR__.'/../lib/ConnectionHandler.php';
require_once __DIR__.'/../lib/database/BlogModel.php';
require_once __DIR__.'/../lib/database/UserModel.php';
require_once __DIR__.'/../lib/database/CategoryModel.php';
require_once __DIR__.'/../lib/database/EntryModel.php';
require_once __DIR__.'/../lib/database/Join.php';
$config = include __DIR__.'/../lib/includes/config.inc.php';

if (!validateLoggedIn()) {
    redirectRegister('../');
}

$BlogModel = new BlogModel();
$UserModel = new UserModel();
$CategoryModel = new CategoryModel();
$EntryModel = new EntryModel();

$user = $UserModel->currentUser();

$id = $_GET['id'];
$blog = $BlogModel->readById($id);

$categories = $CategoryModel->readAll(1000, 'name asc');

$entryJoins = array(
  new Join('category', 'entry', 'categoryId', 'categoryId'),
  new Join('blog', 'entry', 'blogId', 'blogId'),
  new Join('user', 'blog', 'userId', 'userId'),
);
$entries = $EntryModel->joinAndReadAll($entryJoins, 'blog.blogId = '.$id, 1000, '*', false, 'entryId desc');
?>
<!DOCTYPE html>
<html lang="de">
<head>
    <?php printHeader($blog['name'], '../') ?>

    <link rel="stylesheet" href="../css/blog-view-app.css">
    <script src="../js/blog-view_app.js"></script>
</head>
<body>
<div class="container">
    <h1 class="page-header"><?= $blog['name']?>
        <small>Beiträge</small>
    </h1>

    <ul class="breadcrumb">
      <li><a href="../index.php">Blogübersicht</a></li>
      <li class="active"><?= $blog['name'] ?></li>
    </ul>

    <?php

    printUser("../");

    require '../lib/includes/flash.inc.php';

    if (userIsAuthorized($blog['blogId'])) {
        echo <<<EOF
        <div class="margin-btm-10">
        <a class="btn btn-primary btn-sm" data-toggle="modal" data-target="#createEntryModal">Eintrag hinzufügen</a>
        <form action="performBlogAction.php" method="post" class="inline-block">
          <input type="hidden" name="type" value="deleteBlog">
          <input type="hidden" name="blogId" value="$id">
          <button type="submit" class="btn btn-sm btn-danger" id="deleteBlogButton">Blog löschen</button>
        </form>
        </div>
EOF;
    }

    foreach ($entries as $entry): ?>

<div class="panel panel-default">
  <div class="panel-heading">
    <a href="../entry/view.php?id=<?= $entry['entryId']?>" class="title-link"><h3 class="panel-title"><i class="fa fa-link"></i> '<?= strTruncate($entry['title'], 50) ?>' in  <span class="label label-primary"><?= $entry[7]?></span> am <?= date('d.m. \u\m H:i', date_create_from_format(DATE_ATOM, $entry['date'])->getTimestamp()) ?></h3></a>
  </div>
  <div class="panel-body">
    <p>
      <?= strTruncate(nl2br($entry['content']), 550, '...<br/><a href="../entry/view.php?id='.$entry['entryId'].'"><i class="fa fa-external-link-square"></i> Weiterlesen</a>') ?>
    </p>
  </div>

</div>
    <?php endforeach;?>

    <hr/>
</div>


<!-- CREATE ENTRY MODAL -->
<div class="modal fade" id="createEntryModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
                <h4 class="modal-title">Eintrag erstellen</h4>
            </div>
            <div class="modal-body">
                <form action="performBlogAction.php" method="POST" id="createForm">
                    <input type="hidden" name="type" value="post"/>
                    <input type="hidden" name="blogId" value="<?= $id ?>">

                    <div class="form-group">
                      <label for="createFormSelectCategory">Kategorie</label>
                      <select class ="form-control" name="categoryId" id="createFormSelectCategory">
                        <?php foreach ($categories as $category):?>
                          <option value="<?= $category['categoryId'] ?>"><?= $category['name'] ?></option>
                        <?php endforeach; ?>
                      </select>
                    </div>

                    <div class="form-group">
                        <label for="createFormTitle" class="control-label">Titel</label>
                        <input type="text" name="title" id="createFormTitle" class="form-control" placeholder="Titel" required>
                    </div>
                    <div class="form-group">
                        <label for="createFormContent" class="control-label">Inhalt</label>
                        <textarea name="content" id="createFormContent" class="form-control" placeholder="Inhalt" required></textarea>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
                <button type="button" class="btn btn-primary" id="createFormSubmit">Eintrag erstellen</button>
            </div>
        </div>
    </div>
</div>

</body>
</html>
