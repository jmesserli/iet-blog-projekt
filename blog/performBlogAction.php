<?php

require_once __DIR__.'/../lib/includes/helper.inc.php';
require_once __DIR__.'/../lib/ConnectionHandler.php';
require_once __DIR__."/../lib/database/EntryModel.php";
require_once __DIR__."/../lib/database/BlogModel.php";

session_start();

if (!validateLoggedIn()) {
    redirectRegister('../');
}

$EntryModel = new EntryModel();
$BlogModel = new BlogModel();

$type = '';

if (!isset($_POST['type'])) {
    if (!isset($_GET['type'])) {
        echo 'custom request';
        die();
    } else {
        $type = $_GET['type'];
    }
} else {
    $type = $_POST['type'];
}

switch ($type) {
  case 'create':

    if (!isset($_POST['name']) || empty(trim($_POST['name']))) {
        header('Location: ../index.php'.generateFlash('Sie haben keinen Namen eingegeben...'));
        die();
    }

    $query = 'SELECT COUNT(*) FROM blog WHERE name = :name';
    $stmt = ConnectionHandler::getConnection()->prepare($query);
    $stmt->bindParam(':name', $_POST['name'], SQLITE3_TEXT);
    $result = $stmt->execute();

    if ($result) {
        $count = $result->fetchArray()[0];

        if ($count == 0) {
            $query = 'INSERT INTO blog (userId, name) VALUES (:user, :name)';

            $stmt = ConnectionHandler::getConnection()->prepare($query);
            $stmt->bindParam(':user', $_SESSION['userId'], SQLITE3_INTEGER);
            $stmt->bindParam(':name', $_POST['name'], SQLITE3_TEXT);
            $result = $stmt->execute();

            if ($result) {
                header('Location: ../index.php'.generateFlash('Blog erstellt.', 'success'));
            } else {
                header('Location: ../index.php'.generateFlash('Interner Fehler: cant_insert_database, bitte versuchen Sie es später nochmals.', 'danger'));
            }
        } else {
            header('Location: ../index.php'.generateFlash('Ein Blog mit dem Namen '.$_POST['name'].' existiert bereits'));
        }
    } else {
        header('Location: ../index.php'.generateFlash('Interner Fehler: cant_query_database, bitte versuchen Sie es später nochmals.', 'danger'));
    }
    break;

  case 'post':
    if (!assureNonEmpty("POST", "blogId", "categoryId")) {
        header('Location: ../index.php'.generateFlash('Interner Fehler: err_blogid_categoryid_not_set', 'danger'));
        die();
    }

    $blogId = $_POST['blogId'];
    $categoryId = $_POST['categoryId'];

    if (!userIsAuthorized($blogId)){
      header("Location: ../index.php".generateFlash("Du bist nicht autorisiert diesen Vorgang für diesen Blog auszuführen.", 'info'));
      die();
    }

    if (!assureNonEmpty("POST", "title", "content")) {
        header('Location: view.php'.generateFlash('Titel oder Inhalt nicht gesendet.', 'warning')."&id=$blogId");
        die();
    }

    $title = htmlspecialchars($_POST['title']);
    $content = htmlspecialchars($_POST['content']);
    $date = date(DATE_ATOM);

    $query = 'INSERT INTO entry (blogId, categoryId, title, content, date) values (:blogId, :categoryId, :title, :content, :date)';
    $stmt = ConnectionHandler::getConnection()->prepare($query);
    $stmt->bindParam(':blogId', $blogId, SQLITE3_INTEGER);
    $stmt->bindParam(':categoryId', $categoryId, SQLITE3_INTEGER);
    $stmt->bindParam(':title', $title, SQLITE3_TEXT);
    $stmt->bindParam(':content', $content, SQLITE3_TEXT);
    $stmt->bindParam(':date', $date, SQLITE3_TEXT);
    $result = $stmt->execute();

    if (!$result) {
        header('Location: view.php'.generateFlash('Interner Fehler: cant_insert_database', 'danger')."&id=$blogId");
    } else {
        header('Location: view.php'.generateFlash("Blogeintrag '$title' erstellt", 'success')."&id=$blogId");
    }
    break;

  case 'deleteBlog':
    if (!assureNonEmpty("POST", "blogId")){
      header("Location: ../index.php".generateFlash("Interner Fehler: err_blogid_not_set", "danger"));
      die();
    }

    $blogId = $_POST["blogId"];

    if (!userIsAuthorized($blogId)){
      header("Location: ../index.php".generateFlash("Du bist nicht autorisiert diesen Vorgang für diesen Blog auszuführen.", "warning"));
      die();
    }

    $BlogModel->deleteById($blogId);
    header("Location: ../index.php".generateFlash("Blog gelöscht.", "success"));
  break;

  case 'edit':
    if(!assureNonEmpty("POST", "categoryId", "entryId")){
      header('Location: ../index.php'.generateFlash('Interner Fehler: err_entryid_categoryid_not_set', 'danger'));
      die();
    }

    $entry = $EntryModel->readById($_POST["entryId"]);

    if (!userIsAuthorized($entry["blogId"])){
      header("Location: ../index.php".generateFlash("Du bist nicht autorisiert diesen Vorgang für diesen Blog auszuführen.", 'info'));
      die();
    }
    if (!assureNonEmpty("POST", "title", "content")) {
      header("Location: view.php".generateFlash("Titel oder Inhalt nicht gesendet.", "warning")."&id={$entry['blogId']}");
      die();
    }

    // Update post
    $EntryModel->updateFieldById($entry["entryId"], "categoryId", $_POST["categoryId"]);
    $EntryModel->updateFieldById($entry["entryId"], "title", htmlspecialchars($_POST["title"]));
    $EntryModel->updateFieldById($entry["entryId"], "content", htmlspecialchars($_POST["content"]));
    $EntryModel->updateFieldById($entry["entryId"], "date", date(DATE_ATOM));

    header("Location: ../entry/view.php".generateFlash("Post bearbeitet.", "success")."&id={$entry['entryId']}");

    break;

  case 'deleteEntry':
    if(!assureNonEmpty("POST", "entryId")) {
      header("Location: ../index.php".generateFlash("Interner Fehler: err_entryid_not_set","danger"));
      die();
    }
    $entryId = $_POST["entryId"];
    $entry = $EntryModel->readById($entryId);

    if (!userIsAuthorized($entry["blogId"])){
      header("Location: ../index.php".generateFlash("Du bist nich autorisiert diesen Vorgang für diesen Blog auszuführen.", "info"));
      die();
    }

    $EntryModel->deleteById($entryId);

    header("Location: ../blog/view.php".generateFlash("Eintrag gelöscht.", "success")."&id={$entry['blogId']}");
  break;

  default:
    echo 'unsupported action';
    break;
}
